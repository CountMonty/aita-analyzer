## Installing and Running the AITA Analyzer Conda Environment
 The `conda` program is an open source package and environment manager commonly used for Python, and operating at the core of the Anaconda Python distribution.  The YAML file(s) in this directory define a `conda` environment for running the AITA Analyzer python software.

### Installing and Updating Conda
Instructions on installing Ana/Mini conda are here: https://docs.anaconda.com/anaconda/install/.  Make sure Anaconda's `bin` directory is on your path before proceeding.

If Anaconda is already installed, but is not up to date, you can update it with the following commands (the first updates the `conda` program itself, the latter updates the python packages bundled with Anaconda):
```bash
$ conda update conda
$ conda update anaconda
```

### Creating the Conda Environment
Assuming you have an up-to-date version of anaconda installed, you can install or update the environments with the commands below.

```bash
$ cd $AITA_ROOT
$ conda env create -f env/aita.yaml
```

In the above example, the variable `$AITA_ROOT` is assumed to point to the AITA Analyzer repo root.

### Updating the Conda Environment
If the `aita.yaml` file changes for any reason (e.g. adding new packages), the environment may be updated to be consistent with this new YAML file with the following command.

```bash
$ cd $AITA_ROOT
$ conda env update -f env/aita.yaml
```

In the above example, the variable `$AITA_ROOT` is assumed to point to the AITA Analyzer repo root.

### Usage
With Anaconda's `bin` directory on your path, you can activate the environment by typing `conda activate aita` (the environment's name, `aita`, is defined at the top of the YAML file).  Once active, only the packages, and respective versions, installed into the environment will be available when running Python.

Exiting the environment is a simple `conda deactivate` command at the command-line.

```bash
$ conda activate aita
$ conda deactivate
```

### Exporting an Environment

Once created, an environment's definition may be exported to a YAML file again.  The command to do this:
```bash
$ conda env export > environment.yaml
```
This `environment.yaml` file will contain all packages that were installed, which will be a superset on those originally provided, as it will include any dependencies needed by the packages specified in `aita.yaml`.  It will also contain the exact version of the installed packages, (hopefully) allowing an environment to be perfectly reproduced on another machine.
