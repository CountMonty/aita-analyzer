TODO: Format this properly, provide better examples

Imports should be split into two sections: `External` and `Internal`. External are for imports that are external to the project, while internal are those that are, well, internal. Furthermore, of the two styles to importing, `import module` should always be above `from module import item`. Finally, imports must be in alphabetical order by module name.

Example of all practices:
```
# External
import math
import os

from sys import path

# Internal
import utils

from models import zFunc
from utils  import func
from utils  import method
```

Readability is King. If spacing can improve the readability of a section, it's best to do so. As seen above, the `from _ import *` are space aligned on `import` to easily see what is imported from where. Sometimes this is beneficial to variables as well, ie:
```
aa = 1
bbbb = 2
c = 3
```
vs
```
aa   = 1
bbbb = 2
c    = 3
```
This isn't enforced too much, but keep in mind how whitespace can influence readability.

Other good practices include:
* Using f-strings instead of .format
* Taking advantage of utility functions so code is not repeated
* Code for the future when available
