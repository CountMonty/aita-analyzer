"""initialize db

Revision ID: 0a3fea16f138
Revises:
Create Date: 2019-10-03 18:42:46.728921

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '0a3fea16f138'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('posts',
    sa.Column('id', sa.VARCHAR(length=16), nullable=False),
    sa.Column('creation', sa.FLOAT(), nullable=False),
    sa.Column('flair', sa.VARCHAR(length=64), nullable=True),
    sa.Column('judgement', sa.VARCHAR(length=16), nullable=True),
    sa.Column('popularity', sa.INTEGER(), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )

def downgrade():
    op.drop_table('posts')
