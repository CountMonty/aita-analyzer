# External
import os
import sys

from sqlalchemy import distinct
from sqlalchemy import func

sys.path.append(f"{os.getenv('AITA_ROOT')}/src")
# Internal
from database.models import Post
from utils           import createSession
from utils           import cronjob
from utils           import getLog
#from utils           import Slackbot

def report():
    # Instantiate log, slack, and session
    log     = getLog('reporter')
    bot     = Slackbot()
    session = createSession()
    try:
        # Construct the report message
        msg = f'''\
Total posts     = {session.query(func.count(Post.id)).first()[0]}
Total flairs    = {session.query(func.count(Post.flair)).first()[0]}
Total judgments = {session.query(func.count(Post.judgement)).first()[0]}\
'''

        bot.send(msg)
    except Exception as e:
        bot.send('Reporter encountered an exception at {ts}:' + f'\n{e}')
        log.exception('An exception was encountered while watching the new posts stream.')
        print(e)

def main():
    try:
        cronjob(report)
    except (KeyboardInterrupt, SystemExit):
        pass

if __name__ == '__main__':
    main()
