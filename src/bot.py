# External
import os
import sys

from datetime import datetime as dt
from slack    import RTMClient
from slack    import WebClient

sys.path.append(f"{os.getenv('AITA_ROOT')}/src")
# Internal
from utils import parseConfig

class Slackbot(object):
    def __init__(self, *args, **kwargs):
        config = parseConfig()
        self.enabled = False
        try:
            self.token   = config['SLACK']['token']
            self.channel = config['SLACK']['channel']
            self.client  = WebClient(token=self.token)
            self.ts      = None
            self.enabled = True
        except:
            print('Failed to initialize Slack, disabling functionality.')

    def send(self, msg, **kwargs):
        if self.enabled:
            post = {'channel': self.channel,
                  'thread_ts': self.ts,
                       'text': msg.format(ts=dt.now().isoformat(" "))}
            try:
                resp = self.client.chat_postMessage(**post, **kwargs)
                if not resp['ok']:
                    print('Failed to send Slack message')
                return resp['ts']
            except Exception as e:
                print(f'msgSlack() encounted an exception:\n{e}')
                return None


    def request(self, msg, **kwargs):
        '''
        Sends a message to Slack requesting for a reaction to be added to the message as the response

        When the RTM client starts, it will wait until the reaction() func is called by a user adding
        a reaction to a message in the channel. If the ts of that message matches the ts of the desired
        message then it shuts down the RTM client and then sets self.response to the reaction. This allows
        for request() to grab the reaction after rtm.start() releases to return to the calling script.
        '''
        @RTMClient.run_on(event='reaction_added')
        def reaction(**payload):
            # If the correct message was responded to, shutdown RTM and return the response
            if ts == payload['data']['item']['ts']:
                rtm.stop()
                self.response = payload['data']['reaction']

        self.response = None
        # Send the response request message
        ts = self.send(msg)
        #print(f'seeking {ts}')
        # Start the RTM client
        rtm = RTMClient(token=self.token)
        rtm.start()

        return self.response
