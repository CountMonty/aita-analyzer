# External
import logging
import os
import praw
import signal
import sys

from apscheduler.schedulers.blocking import BlockingScheduler

from configparser   import ConfigParser
from functools      import partial
from sqlalchemy     import create_engine
from sqlalchemy.orm import sessionmaker

try:
    # Optional packages
    from bot import Slackbot
except:
    pass

sys.path.append(f"{os.getenv('AITA_ROOT')}/src")
# Internal


def parseConfig():
    config = ConfigParser()
    config.read('config.ini')
    if config:
        return config
    else:
        print('Could not retrieve config file, exiting')
        sys.exit()

def redditClient():
    config = parseConfig()

    reddit = praw.Reddit(
        client_id     = config['REDDIT']['clientID'],
        client_secret = config['REDDIT']['clientSecret'],
        user_agent    = config['REDDIT']['userAgent'],
        username      = config['REDDIT']['username'],
        password      = config['REDDIT']['password']
    )

    return reddit

def createSession():
    from database.db import Base

    config = parseConfig()

    engine = create_engine(config['DB']['posts'])
    Base.metadata.bind = engine

    dbSession = sessionmaker(bind=engine)
    session = dbSession()

    return session

def getLog(name=None):
    log = logging.getLogger(name)
    log.setLevel(logging.INFO)

    ch = logging.StreamHandler()
    formatter = logging.Formatter(fmt='%(asctime)s\t %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    ch.setFormatter(formatter)
    ch.setLevel(logging.INFO)
    log.addHandler(ch)

    fh = logging.FileHandler(f"{os.getenv('AITA_ROOT')}/logs/{name}.log")
    fh.setFormatter(formatter)
    log.addHandler(fh)

    return log

def dieGracefully(*args, session, slack=None, **kwargs):
    print('Ctrl-C caught, committing session and exiting...')
    session.commit()
    if slack:
        Slackbot().send(slack)
    sys.exit()

def makePartial(session, slack=False):
    return partial(dieGracefully, session=session, slack=slack)

def cronjob(func):
    scheduler = BlockingScheduler()
    scheduler.add_job(func, 'cron', minute=0, hour=0)
    scheduler.start()
