# External
import os
import sys

from datetime   import datetime
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Float
from sqlalchemy import Integer
from sqlalchemy import String

sys.path.append(f"{os.getenv('AITA_ROOT')}/src")
# Internal
from database.db import Base

class Post(Base):
    __tablename__ = 'posts'
    id         = Column(String(16), primary_key=True)
    creation   = Column(Float, nullable=False)
    flair      = Column(String(64))
    judgement  = Column(String(16))
    popularity = Column(Integer)

    def elapse(self):
        return datetime.utcnow() - datetime.utcfromtimestamp(self.creation)

    def __repr__(self):
        return f'<Post(id={self.id}, flair={self.flair})>'
