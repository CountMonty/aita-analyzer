# External
import os
import sys

from sqlalchemy                 import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid      import hybrid_property

sys.path.append(f"{os.getenv('AITA_ROOT')}/src")
# Internal
#from utils import parseConfig

Base = declarative_base()

#config = parseConfig()
#if config:
#    engine = create_engine(config['DB']['posts'])
#    Base.metadata.create_all(engine)
#else:
#    print('Unable to bind database')
