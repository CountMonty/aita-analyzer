# External
import os
import pickle
import signal
import sys

sys.path.append(f"{os.getenv('AITA_ROOT')}/src")
# Internal
from bot import Slackbot
from database.models import Post
from utils  import createSession
from utils  import getLog
from utils  import makePartial
from utils  import redditClient

def askUser(post):
    response = Bot.request(
    f'''
Judgement has encountered an unmapped flair: `{post.flair}`
Please add a reaction to this post to map it:
>:one: - YTA
>:two: - NTA
>:three: - NAH
>:four: - ESH
>:coffin: - Delete
Judgement will not move on until this message has been answered.
    '''
    )

    options = {
        'one': 'YTA',
        'two': 'NTA',
        'three': 'NAH',
        'four': 'ESH'
    }
    if response in options:
        # Update the post
        verdict = options[response]
        post.judgement = verdict
        Log.info(f'Updating {post.id}.judgement = {verdict}')
    elif response == 'coffin':
        verdict = 'DEL'
        Session.delete(post)

    # Update the map file
    global Map
    if verdict not in Map:
        Map[verdict] = []
    Map[verdict].append(post.flair)
    with open('judgements.map', 'wb') as file:
        pickle.dump(Map, file)


def judge():
    posts = Session.query(Post).filter(Post.flair != None, Post.judgement == None).all()
    Log.info(f'Queried {len(posts)} posts')
    for post in posts:
        # Find the appropriate judgement
        for verdict, aliases in Map.items():
            if post.flair in aliases:
                Log.info(f'Updating {post} to {verdict}')
                post.judgement = verdict
                break
        # If not found, ask
        if not post.judgement:
            askUser(post)
    Session.commit()

def main():
    global Log, Session, Map, Bot

    Session = createSession()
    Log = getLog('judgement')
    Bot = Slackbot()

    with open('judgements.map', 'rb') as file:
        Map = pickle.load(file)

    # Setup Ctrl-C catcher
    dieGracefully = makePartial(Session)
    signal.signal(signal.SIGINT, dieGracefully)

    judge()

if __name__ == '__main__':
    main()
