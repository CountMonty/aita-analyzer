# External
import multiprocessing as mp
import os
import signal
import sys

sys.path.append(f"{os.getenv('AITA_ROOT')}/src")
# Internal
import judgement
import reporter
import preprocessing.updater as updater
import preprocessing.watcher as watcher

def start():
    pass

def kill():
    pass

def exit(*args, **kwargs):
    print('Caught ctrl-c, sending SIGINT to children processes')
    for _, p in Procs.items():
        os.kill(p.pid, signal.SIGINT)
    # Join to wait for them to finish
    [p.join() for _, p in Procs.items()]
    print('Finished shutting down children, exiting')
    sys.exit()

def administrate():
    while True:
        action = input('''
Select an action:
    s - Start | Starts a script process. Will restart a script if it is already running.
    k - Kill  | Sends SIGINT to a child process.
    q - Quit  | Sends SIGINT to all child processes then exits.

Selection: '''
        )
        # TODO: Implement these, fix exit() to send SIGINT instead of SIGTERM (psutils?)
        if action == 's':
            start()
        elif action == 'k':
            kill()
        elif action == 'q':
            exit()
            #raise KeyboardInterrupt

if __name__ == '__main__':
    scripts = input('''\
Please select which scripts to run:
    1 - Watcher
    2 - Updater
    3 - Judgement
    4 - Reporter
More than one script can be selected by combining their numbers, eg: 1234.

Selection: '''
    )

    Procs = {}
    if '1' in scripts:
        p = mp.Process(target=watcher.main)
        p.start()
        Procs['watcher'] = p
    if '2' in scripts:
        p = mp.Process(target=updater.main)
        p.start()
        Procs['updater'] = p
    if '3' in scripts:
        p = mp.Process(target=judgement.main)
        p.start()
        Procs['judgement'] = p
    if '4' in scripts:
        p = mp.Process(target=reporter.main)
        p.start()
        Procs['reporter'] = p

    # Setup ctrl-c catcher to kill the child processes
    signal.signal(signal.SIGINT, exit)

    administrate()

# TODO: Figure out a way to split the terminal similar to https://stackoverflow.com/questions/39000250/how-to-display-two-different-outputs-in-python-console
