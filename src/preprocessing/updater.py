# External
import os
import signal
import sys

sys.path.append(f"{os.getenv('AITA_ROOT')}/src")
# Internal
from database.models import Post
from utils import createSession
from utils import cronjob
from utils import getLog
from utils import makePartial
from utils import redditClient
#from utils import Slackbot

def process():
    # Instantiate
    session = createSession()
    reddit  = redditClient()
    log     = getLog('updater')
    #bot     = Slackbot()
    # Setup Ctrl-C catcher
    dieGracefully = makePartial(session)
    signal.signal(signal.SIGINT, dieGracefully)
    # Start logs
    #bot.send('Starting updater at {ts}')
    log.info('Starting updater')
    # Get those that have not been judged
    posts = session.query(Post).filter(Post.flair == None).all()
    log.info(f'Queried {len(posts)} posts')
    for post in posts:
        # A post shall be judged on its 21st birthhour
        elapsed = post.elapse()
        if elapsed.days or elapsed.seconds / 3600 > 20:
            subm = reddit.submission(id=post.id)
            flair = subm.link_flair_text
            score = subm.score
            # If they have earned their honour, they shall live
            if score > 20 and flair:
                log.info(f'Update: {post.id}\n\t| flair = {flair}\n\t| score = {score}')
                post.flair = flair
                post.popularity = score
            # Otherwise, punishment is death
            else:
                log.info(f'Delete: {post.id}\n\t| flair = {flair}\n\t| score = {score}')
                session.delete(post)
    # Commit thy judgements
    session.commit()

def main():
    #cronjob(process)
    process()

if __name__ == '__main__':
    main()
