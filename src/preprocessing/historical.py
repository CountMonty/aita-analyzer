# External
import json
import os
import requests
import signal
import sys
import time

from datetime import datetime
from datetime import timedelta

sys.path.append(f"{os.getenv('AITA_ROOT')}/src")

# Internal
from database.models import Post
from utils import createSession
from utils import getLog
from utils import makePartial
from utils import redditClient

def get_old_submissions():
    # Size of the intervals (in days) that are queried (size is too large if there are
    # more than 1000 results in that time period):
    step_size = 7

    # Initializing dates:
    before_date = datetime.today() - timedelta(days=1)
    after_date = before_date - timedelta(days=step_size)
    data = data = get_data(before_date, after_date)

    count_ids = 0
    ids_list = []
    while len(data) != 0:
        for i in range(len(data)):
            ids_list.append(data[i]['id'])

        log.info(f'Queried {len(ids_list)} historical posts')


        add_posts_to_db(ids_list)

        before_date = after_date
        after_date = before_date - timedelta(days=step_size)
        data = get_data(before_date, after_date)

        # Already added this intervals posts to the DB
        ids_list = []


def get_data(before_date, after_date):
    # Name of the subreddit to scrape:
    sub = 'AmITheAsshole'
    # Only returns posts with scores >= the limit
    score_limit = 20
    # Getting the offsets:
    before_date_offset = str(before_date.timestamp())[0:10]
    after_date_offset = str(after_date.timestamp())[0:10]
    # Build the URL
    url = f'https://api.pushshift.io/reddit/search/submission/?size=1000&subreddit={sub}&before={before_date_offset}&after={after_date_offset}&sort_type=score&score=>{score_limit}'
    print(url)
    # Making the request of the pushshift.io API
    r = requests.get(url)
    # returns empty list if request was unsuccessful
    try:
        data = json.loads(r.text)
        return data['data']
    except:
        return []


def add_posts_to_db(ids_list):

    for subm_id in ids_list:
        submission = reddit.submission(subm_id)

        # Ignore update posts
        if 'UPDATE' in submission.title:
            continue
        # Ignore meta posts
        if 'META' in submission.title:
            continue

        if submission.id and submission.created_utc:
            # Make sure the post doesn't already exist
            if session.query(Post).filter(Post.id == submission.id).first():
                continue

            # Create the post and add it to the db
            post = Post(id=submission.id, creation=submission.created_utc)
            session.add(post)
            session.commit()
            log.info(f'Added Post: {post.id}')


if __name__ == '__main__':
    reddit  = redditClient()
    session = createSession()
    log     = getLog('historical')

    # Setup Ctrl-C catcher
    dieGracefully = makePartial(session)
    signal.signal(signal.SIGINT, dieGracefully)

    get_old_submissions()
