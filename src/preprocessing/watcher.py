# External
import os
import signal
import sys

sys.path.append(f"{os.getenv('AITA_ROOT')}/src")
# Internal
from database.models import Post
from utils import createSession
from utils import getLog
from utils import makePartial
from utils import redditClient
#from utils import Slackbot

def track(submission):
    # Ignore update posts
    if 'UPDATE' in submission.title:
        return

    # Ignore meta posts
    if 'META' in submission.title:
        return

    if submission.id and submission.created_utc:
        # Make sure the post doesn't already exist
        if Session.query(Post).filter(Post.id == submission.id).first():
            return

        # Create the post and add it to the db
        post = Post(id=submission.id, creation=submission.created_utc)
        Session.add(post)
        Session.commit()
        Log.info(f'Added Post: {post.id}')

def watch(subreddit):
    for submission in subreddit.stream.submissions():
        track(submission)

def main():
    global Log, Session

    Session = createSession()
    # Setup Ctrl-C catcher
    dieGracefully = makePartial(Session, slack='Watcher exited gracefully at {ts}')
    signal.signal(signal.SIGINT, dieGracefully)

    reddit = redditClient()
    Log    = getLog('watcher')
    #bot    = Slackbot()

    # Track new submissions to the subreddit, auto restart on failure
    subreddit = reddit.subreddit('AmITheAsshole')
    while True:
        try:
            #bot.send('Starting watcher at {ts}')
            Log.info('Starting watcher')
            watch(subreddit)
        except Exception as e:
            #bot.send('Watcher encountered an exception at {ts}:' + f'\n{e}')
            Log.exception('An exception was encountered while watching the new posts stream.')

if __name__ == '__main__':
    main()
